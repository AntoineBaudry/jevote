# ** JeVote ** #

JeVote est un projet réalisé en première année de stage.

## **Cas d'utilisation** ##


![casUtilisationJeVote.png](https://bitbucket.org/repo/9q4k7p/images/2284533871-casUtilisationJeVote.png)

### Contexte ###

JeVote est une **application Android** qui permet de **créer un vote** sur son téléphone et d'afficher, en **temps réel les résultats**. Les votes se font par SMS (sur le téléphone où l'application est installée).


### Outils et Logiciels utilisés ###

* Windev Mobile
* Téléphone Android de test


### Description ###

Au début, j'ai du alimenter la base de données (cf Annexe 1) avec les informations remplies par le créateur du vote (cf Annexe 2). 

Ensuite, j'ai du coder une procédure SMSTraitement (cf Annexe 3) qui permet de lire les SMS du téléphone concernant le vote, en gérant différent cas : 

* Vérifier que les conditions ( le temps du vote ou le nombre de votants maximal ne sont pas atteints)
* Autoriser un numéro à voter une fois (si son vote est valide)
* Vérifier que le SMS contient bien un choix possible (choisi par le créateur du vote)
* Supprimer du téléphone uniquement les messages concernant le vote (contenant que un chiffre)

Pour finir sur cette application, j'ai fais une série de test pour trouver et corriger la maximum de bugs.


### Annexes ###

**Annexe 1 : La base de données**


!![baseSQLJeVote.png](https://bitbucket.org/repo/9q4k7p/images/1149544741-baseSQLJeVote.png)



**Annexe 2 : Fenêtre de création de vote**


![fenetrePropriétés.png](https://bitbucket.org/repo/9q4k7p/images/1571615635-fenetrePropri%C3%A9t%C3%A9s.png)



**Annexe 3 : Fenêtre de résultat**


![exemple 1-2.png](https://bitbucket.org/repo/9q4k7p/images/3637847556-exemple%201-2.png)